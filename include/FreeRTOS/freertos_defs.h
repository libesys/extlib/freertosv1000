/*!
* \file freertos_defs.h
* \brief Definitions needed for freeRTOS windows DLL
*
* \cond
* __legal_b__
* __legal_e__
* \endcond
*
*/

#pragma once

#ifdef FREERTOS_EXPORTS
#define FREERTOS_API __declspec(dllexport)
#define FREERTOS_TEMPLATE
#elif FREERTOS_USE
#define FREERTOS_API __declspec(dllimport)
#define FREERTOS_TEMPLATE extern
#else
#define FREERTOS_API
#define FREERTOS_TEMPLATE
#endif

#ifdef _MSC_VER
#pragma warning (disable : 4251)
#pragma warning (disable : 4231)

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#endif

#include "FreeRTOS/autolink.h"