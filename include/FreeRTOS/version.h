/*!
 * \file freertos/version.h
 * \brief Version info for freertos
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __FREERTOS_VERSION_H__
#define __FREERTOS_VERSION_H__

// Bump-up with each new version
#define FREERTOS_MAJOR_VERSION    0
#define FREERTOS_MINOR_VERSION    0
#define FREERTOS_RELEASE_NUMBER   1
#define FREERTOS_VERSION_STRING   _T("freertos 0.0.1")

// Must be updated manually as well each time the version above changes
#define FREERTOS_VERSION_NUM_DOT_STRING   "0.0.1"
#define FREERTOS_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define FREERTOS_VERSION_NUMBER (FREERTOS_MAJOR_VERSION * 1000) + (FREERTOS_MINOR_VERSION * 100) + FREERTOS_RELEASE_NUMBER
#define FREERTOS_BETA_NUMBER      1
#define FREERTOS_VERSION_FLOAT FREERTOS_MAJOR_VERSION + (FREERTOS_MINOR_VERSION/10.0) + (FREERTOS_RELEASE_NUMBER/100.0) + (FREERTOS_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define FREERTOS_CHECK_VERSION(major,minor,release) \
    (FREERTOS_MAJOR_VERSION > (major) || \
    (FREERTOS_MAJOR_VERSION == (major) && FREERTOS_MINOR_VERSION > (minor)) || \
    (FREERTOS_MAJOR_VERSION == (major) && FREERTOS_MINOR_VERSION == (minor) && FREERTOS_RELEASE_NUMBER >= (release)))

#endif

