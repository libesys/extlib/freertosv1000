/*!
* \file freertos_app.h
* \brief Definitions needed for freeRTOS windows DLL
*
* \cond
* __legal_b__
* __legal_e__
* \endcond
*
*/

#ifndef __FREERTOS_APP_H__
#define __FREERTOS_APP_H__

#include "FreeRTOS/freertos_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
* Some of the RTOS hook (callback) functions only need special processing when
* the full demo is being used.  The simply blinky demo has no special
* requirements, so these functions are called from the hook functions defined
* in this file, but are defined in main_full.c.
*/
FREERTOS_API void vFullDemoTickHookFunction(void);
FREERTOS_API void vFullDemoIdleFunction(void);

/*
* This demo uses heap_5.c, so start by defining some heap regions.  This is
* only done to provide an example as this demo could easily create one large
* heap region instead of multiple smaller heap regions - in which case heap_4.c
* would be the more appropriate choice.  No initialisation is required when
* heap_4.c is used.
*/
FREERTOS_API void  prvInitialiseHeap(void);

/*
* Prototypes for the standard FreeRTOS callback/hook functions implemented
* within this file.
*/
FREERTOS_API void vApplicationMallocFailedHook(void);
FREERTOS_API void vApplicationIdleHook(void);
FREERTOS_API void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName);
FREERTOS_API void vApplicationTickHook(void);

typedef void(*ApplicationMallocFailedCallback_t)(void);

FREERTOS_API void vSetApplicationMallocFailedCallback(ApplicationMallocFailedCallback_t xCallBack);

typedef void(*ApplicationIdleCallback_t)(void);

FREERTOS_API void vSetApplicationIdleCallback(ApplicationIdleCallback_t xCallBack);

typedef void(*ApplicationStackOverflowCallback_t)(TaskHandle_t pxTask, char *pcTaskName);

FREERTOS_API void vSetApplicationStackOverflowCallback(ApplicationStackOverflowCallback_t xCallBack);

typedef void(*ApplicationTickCallback_t)(void);

FREERTOS_API void vSetApplicationTickCallback(ApplicationTickCallback_t xCallBack);

typedef void(*AssertCalledCallback_t)(unsigned long ulLine, const char * const pcFileName);

FREERTOS_API void vSetAssertCalledCallback(AssertCalledCallback_t xCallBack);

/*
* Writes trace data to a disk file when the trace recording is stopped.
* This function will simply overwrite any trace files that already exist.
*/
FREERTOS_API void prvSaveTraceFile(void);

#ifdef __cplusplus
}
#endif

#endif